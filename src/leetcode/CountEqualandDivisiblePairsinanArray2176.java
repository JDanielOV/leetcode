package leetcode;

public class CountEqualandDivisiblePairsinanArray2176 {
    public static void main(String[] args) {
        CountEqualandDivisiblePairsinanArray2176 c = new CountEqualandDivisiblePairsinanArray2176();
        System.out.println(c.countPairs(new int[]{3, 1, 2, 2, 2, 1, 3}, 2));
        System.out.println(c.countPairs(new int[]{1, 2, 3, 4}, 1));
    }

    public int countPairs(int[] nums, int k) {
        int iContador = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] == nums[j] && (i * j) % k == 0) {
                    iContador++;
                }
            }
        }
        return iContador;
    }
}
