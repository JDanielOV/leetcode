package leetcode;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class LinkedListComponents817 {

    public void LinkedListComponents817() {
        ArrayList<Integer> head = new ArrayList(Arrays.asList(0, 1, 2, 3, 4));
        int G[] = {0, 3, 1, 4};
        ArrayList<Integer> aNewG = toArrayList(G);
        ArrayList<Integer> aPositions = new ArrayList<>();
        int iCounter=0,iSize=0;
        for (int i : head) {
            int iPosition = aNewG.indexOf(i);
            if (iPosition >= 0) {
                aPositions.add(iCounter);
            }
            iCounter++;
        }
        aPositions.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer integer, Integer t1) {
                return integer-t1;
            }
        });
        iSize=aPositions.size();
        for (int i = 0; i < aPositions.size()-1; i++) {
            if(aPositions.get(i+1)-aPositions.get(i)==1)
            {
                iSize--;
            }
        }
    }

    public ArrayList<Integer> toArrayList(int G[]) {
        List<Integer> lAux = Arrays.stream(G).boxed().collect(Collectors.toList());
        return (ArrayList<Integer>) lAux;
    }

    public static void main(String[] args) {
        LinkedListComponents817 v = new LinkedListComponents817();
        v.LinkedListComponents817();
    }
}
/*
[0,4,2,1,3]
        [1,2,3]
        3,2,4
        ordenamos:
        2,3,4
        arr[i+1]-arr[i]==1

        [1,4,5,2,0,3]
        [1,4,3,2]
        0,1,5,3
        0,1,3,5*/
