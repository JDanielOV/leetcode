/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Daniel Ochoa
 */
public class OccurrencesAfterBigram1078 {

    public String[] OccurrencesAfterBigram1078() {
        String text = "ypkk lnlqhmaohv lnlqhmaohv lnlqhmaohv ypkk ypkk ypkk ypkk ypkk ypkk lnlqhmaohv lnlqhmaohv lnlqhmaohv lnlqhmaohv ypkk ypkk ypkk lnlqhmaohv lnlqhmaohv ypkk", first = "lnlqhmaohv", second = "ypkk";
        String[] sWords = text.split(" ");
        ArrayList<String> aWords = new ArrayList();
        for (int i = 1; i < sWords.length; i++) {
            if (sWords[i].equals(second) && sWords[i - 1].equals(first) && i + 1 < sWords.length) {
                aWords.add(sWords[i + 1]);
            }
        }

        return aWords.toArray(new String[aWords.size()]);
    }
}

//"ypkk lnlqhmaohv lnlqhmaohv lnlqhmaohv ypkk ypkk ypkk ypkk ypkk ypkk lnlqhmaohv lnlqhmaohv lnlqhmaohv lnlqhmaohv ypkk ypkk ypkk lnlqhmaohv lnlqhmaohv ypkk"
//"lnlqhmaohv"
//"ypkk"
