/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class MaximumAscendingSubarraySum1800 {

    public void MaximumAscendingSubarraySum1800() {
        int[] nums = {100,10,1};
        int iSum = nums[0], iMax = iSum;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > nums[i - 1]) {
                iSum += nums[i];
            } else {
                iSum = nums[i];
            }
            if (iSum > iMax) {
                iMax = iSum;
            }
        }
        System.out.println(iMax);
    }
}
