/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;

/**
 *
 * @author Daniel Ochoa
 */
public class ComplementofBase10Integer1009 {
    
    public void ComplementofBase10Integer1009() {
        int N = 10;
        System.out.println(cambio(Integer.toBinaryString(N)));
        System.out.println(Integer.parseInt(cambio(Integer.toBinaryString(N)), 2));
//        System.out.println(Integer.parseInt(Integer.toString(Integer.parseInt(cambio(Integer.toBinaryString(N))), 2)));
    }
    
    public String cambio(String sValue) {
        char[] split = sValue.toCharArray();
        int i = 0;
        for (char c : split) {
            if (c == '1') {
                split[i] = '0';
            } else {
                split[i] = '1';
            }
            i++;
        }
        sValue = String.valueOf(split);
        return sValue;
    }
}
