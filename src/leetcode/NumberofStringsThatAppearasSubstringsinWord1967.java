package leetcode;

import java.util.Arrays;

public class NumberofStringsThatAppearasSubstringsinWord1967 {
    public static void main(String[] args) {
        NumberofStringsThatAppearasSubstringsinWord1967 n = new NumberofStringsThatAppearasSubstringsinWord1967();
        System.out.println(n.numOfStrings(new String[]{"a", "abc", "bc", "d"}, "abc"));
        System.out.println(n.numOfStrings(new String[]{"a", "b", "c"}, "aaaaabbbbb"));
    }

    public int numOfStrings(String[] patterns, String word) {
        return (int) Arrays.stream(patterns).filter(word::contains).count();
    }
}
