package leetcode;

import java.util.Arrays;

public class MinimumSumofFourDigitNumberAfterSplittingDigit2160 {
    public static void main(String[] args) {
        MinimumSumofFourDigitNumberAfterSplittingDigit2160 v = new MinimumSumofFourDigitNumberAfterSplittingDigit2160();
        System.out.println(v.minimumSum(1234));
    }

    public int minimumSum(int num) {
        String[] sNumberos = (String.valueOf(num).split(""));
        Arrays.sort(sNumberos);
        return (Integer.parseInt(sNumberos[0] + sNumberos[3]) + Integer.parseInt(sNumberos[1] + sNumberos[2]));
    }
}
