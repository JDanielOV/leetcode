/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.ArrayList;

/**
 *
 * @author Daniel Ochoa
 */
public class KeyboardRow500 {

    public String[] KeyboardRow500() {
        String[] words = {"Hello", "Alaska", "Dad", "Peace"};
        String[] sKeyboard = {"qwertyuiop", "asdfghjkl", "zxcvbnm"};
        ArrayList<String> aList = new ArrayList();
        for (String word : words) {
            boolean bControl = true;
            char cValueZero = word.charAt(0);
            String sReturnSearch = searchRow(cValueZero);
            for (char c : word.toCharArray()) {
                if (!sReturnSearch.contains(("" + c).toLowerCase())) {
                    bControl = false;
                }
            }
            if (bControl) {
                aList.add(word);
            }
        }
        for (String string : aList) {
            System.out.println(string);
        }
        words = new String[aList.size()];
        return aList.toArray(words);
    }

    public String searchRow(char cValue) {
        String[] sKeyboard = {"qwertyuiop", "asdfghjkl", "zxcvbnm"};
        for (String string : sKeyboard) {
            if (string.contains(("" + cValue).toLowerCase())) {
                return string;
            }
        }
        return "";
    }
}
