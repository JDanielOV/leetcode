/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class SumofEvenNumbersAfterQueries985 {

    public static void SumofEvenNumbersAfterQueries985() {
        int[] A = {11, 23, 3, 4};
        int[][] queries = {{32, 1}, {23, 0}, {210, 3}, {4, 2}};
        int iSum = 0;
        for (int query : A) {
            iSum += (Math.abs(query) % 2 == 0) ? query : 0;
        }
        StringBuilder x=new StringBuilder();
        System.out.println("iSum = " + iSum);
        for (int i = 0; i < queries.length; i++) {
            int xx = A[queries[i][1]];
            int z;
            if (Math.abs(xx) % 2 == 0) {
                z = xx + queries[i][0];
                if (Math.abs(z) % 2 == 0) {
                    iSum += queries[i][0];
                } else {
                    iSum -= xx;
                }
            } else {
                z = xx + queries[i][0];
                if (Math.abs(z) % 2 == 0) {
                    iSum += z;
                } else {

                }
            }
            A[i] = iSum;
        }
        for (int iResult : A) {
            System.out.print(iResult + " ");
        }
        System.out.println("iSum = " + iSum);
    }

    public static void SumofEvenNumbersAfterQueries985_() {
        int[] A = {11, 23, 3, 4};
        int[][] queries = {{32, 1}, {23, 0}, {210, 3}, {4, 2}};
    }
}
