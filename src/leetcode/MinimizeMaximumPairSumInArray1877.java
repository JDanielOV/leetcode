package leetcode;

import java.util.Arrays;

public class MinimizeMaximumPairSumInArray1877 {
    public int minPairSum(int[] nums) {
        Arrays.sort(nums);
        int iMaxValue = 0;
        for (int i = 0; i < nums.length / 2; i++) {
            int iValue = nums[i] + nums[nums.length - 1 - i];
            iMaxValue = Math.max(iMaxValue, iValue);
        }
        return iMaxValue;
    }

    public static void main(String[] args) {
        MinimizeMaximumPairSumInArray1877 v = new MinimizeMaximumPairSumInArray1877();
        int[] i = {3,5,4,2,4,6};
        System.out.println(v.minPairSum(i));
    }
}
