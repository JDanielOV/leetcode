package leetcode;

public class RomantoInteger13 {
    public static void main(String[] args) {
        System.out.println(romanToInt("MCMXCIV"));//vicxmcm   iiiv
    }

    public static int romanToInt(String s) {
        int iOutput = 0;
        for (int i = 0; i < s.length(); i++) {
            if (i + 1 < s.length() && values(s.charAt(i)) > values(s.charAt(i + 1))) {
                iOutput += values(s.charAt(i));
            } else if (i + 1 < s.length() && values(s.charAt(i)) < values(s.charAt(i + 1))) {
                iOutput += (values(s.charAt(i + 1)) - values(s.charAt(i)));
                i++;
            } else {
                iOutput += values(s.charAt(i));
            }
        }
        return iOutput;
    }

    public static int values(char cValue) {
        int[] iValues = {1, 5, 10, 50, 100, 500, 1000};
        char[] cValues = {'I', 'V', 'X', 'L', 'C', 'D', 'M'};
        for (int i = 0; i < cValues.length; i++) {
            if (cValue == cValues[i])
                return iValues[i];
        }
        return 0;
    }
}
