package leetcode;

import java.util.ArrayList;

public class NumberofLaserBeamsinaBank2125 {
    public static void main(String[] args) {
        NumberofLaserBeamsinaBank2125 v = new NumberofLaserBeamsinaBank2125();
        System.out.println(v.numberOfBeams(new String[]{"1101", "0111", "1110", "0100", "0110"}));
    }

    public int numberOfBeams(String[] bank) {
        ArrayList<String> aValores = new ArrayList<>();
        int iSumLaser = 0;
        for (String s :
                bank) {
            if (s.contains("1")) {
                aValores.add(s);
            }
        }
        for (int i = 0; i < aValores.size() - 1; i++) {
            int iPrimerPiso = aValores.get(i).replace("0", "").length();
            int iSegundoPiso = aValores.get(i + 1).replace("0", "").length();
            iSumLaser += iPrimerPiso * iSegundoPiso;
        }
        return iSumLaser;
    }
    /*
        ["010","010","010"]
        ["100","010","001"]
        ["100","011","000"]
        ["100","000","101"]
        ["100","100","000","001"]
        ["1101","0111","1110","0100","0110"]
        */

}
