package leetcode;

public class ExecutionofAllSuffixInstructionsStayinginaGrid2120 {
    public static void main(String[] args) {
        ExecutionofAllSuffixInstructionsStayinginaGrid2120 v = new ExecutionofAllSuffixInstructionsStayinginaGrid2120();
        int[] iResultado = v.executeInstructions(3, new int[]{0, 1}, "RRDDLU");
    /*    for (int i :
                iResultado) {
            System.out.println(i);
        }*/
    }

    public int[] executeInstructions(int n, int[] startPos, String s) {
        int iLongitudMovimientos = s.length();
        int[] iMovimientosTotales = new int[iLongitudMovimientos];
        for (int i = 0; i < iLongitudMovimientos; i++) {

            int iContadorMovimientosValidos = 0, iRenglon = startPos[0], iColumna = startPos[1];

            for (int j = i; j < iLongitudMovimientos; j++) {

                char cMovimiento = s.charAt(j);

                switch (cMovimiento) {
                    case 'L' -> {
                        iColumna--;
                    }
                    case 'R' -> {
                        iColumna++;
                    }
                    case 'U' -> {
                        iRenglon--;
                    }
                    case 'D' -> {
                        iRenglon++;
                    }
                }
                //    System.out.println("Cordenada nueva -> X: " + iRenglon + " Y: " + iColumna);
                if (!(iColumna <= n - 1 && iRenglon <= n - 1 && iColumna >= 0 && iRenglon >= 0)) {
                    break;
                } else {
                    iContadorMovimientosValidos++;
                }
            }
            //System.out.println(iContadorMovimientosValidos);
            iMovimientosTotales[i] = iContadorMovimientosValidos;
        }
        return iMovimientosTotales;
    }
}
