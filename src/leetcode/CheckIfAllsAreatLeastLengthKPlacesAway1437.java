/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;

/**
 *
 * @author Daniel Ochoa
 */
public class CheckIfAllsAreatLeastLengthKPlacesAway1437 {

    public void CheckIfAllsAreatLeastLengthKPlacesAway1437() {
        int[] nums = {1, 0, 0, 0, 1, 0, 0, 1};
        int k = 2;
        int iUltimoUno = searchFirstOne(nums);
        for (int i = iUltimoUno + 1; i < nums.length; i++) {
            if (nums[i] == 1) {
                if (i - (iUltimoUno + 1) >= k) {
                    iUltimoUno = i;
                } else {
//                    return false;
                }
            }
        }
    }

    public int searchFirstOne(int[] iValues) {
        for (int i = 0; i < iValues.length; i++) {
            if (iValues[i] == 1) {
                return i;
            }
        }
        return -1;
    }
}
/*
[1,2]     [1,3,5,7]
[3,4]---> [2,4,6,8]
[5,6]
[7,8]
*/