package leetcode;

public class SubrectangleQueries1476 {

    private int[][] iValores;

    public static void main(String[] args) {
        int[][] ivalores = {
                {2, 4, 5, 5},
                {8, 3, 1, 0},
                {4, 6, 9, 2},
                {1, 5, 8, 2}
        };
        SubrectangleQueries1476 v = new SubrectangleQueries1476(ivalores);
        v.updateSubrectangle(1, 1, 2, 2, 0);
        v.updateSubrectangle(0, 0, 3, 0, 0);
        v.updateSubrectangle(2, 0, 2, 3, 0);
        v.updateSubrectangle(0, 0, 3, 3, 0);
    }

    public SubrectangleQueries1476(int[][] rectangle) {
        this.iValores = rectangle;
    }

    public void updateSubrectangle(int row1, int col1, int row2, int col2, int newValue) {
        for (int i = row1; i <= row2; i++) {
            for (int j = col1; j <= col2; j++) {
                this.iValores[i][j] = newValue;
            }
        }
        /*for (int i = row1; i <= row2; i++) {
            for (int j = col1; j <= col2; j++) {
                System.out.print(this.iValores[i][j] + " ");
            }
            System.out.println();
        }*/
    }

    public int getValue(int row, int col) {
        return this.iValores[row][col];
    }

    /*
     *   2   4   5   5
     *   8   3   1   0
     *   4   6   9   2
     *   1   5   8   2
     * */
}
