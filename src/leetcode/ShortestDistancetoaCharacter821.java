/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class ShortestDistancetoaCharacter821 {

    public void ShortestDistancetoaCharacter821() {
        String s = "aaab";
        char c = 'b';
        int[] iPositions = new int[s.length()];
        StringBuilder sText = new StringBuilder(s);
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == c) {
                iPositions[i] = 0;
            } else {
                int iRigth = sText.substring(i).indexOf("" + c);
                int iLeft = new StringBuilder(sText.substring(0, i + 1)).reverse().indexOf("" + c);
                if (iRigth != -1 && iLeft != -1) {
                    iPositions[i] = Math.min(iRigth, iLeft);
                } else if (iRigth != -1) {
                    iPositions[i] = iRigth;
                } else {
                    iPositions[i] = iLeft;
                }
            }
        }
        for (int iPosition : iPositions) {
            System.out.print(iPosition + " ");
        }
    }
}
