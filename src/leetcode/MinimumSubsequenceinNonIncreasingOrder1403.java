/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;
import java.util.ArrayList;

/**
 *
 * @author Daniel Ochoa
 */
public class MinimumSubsequenceinNonIncreasingOrder1403 {

    public void MinimumSubsequenceinNonIncreasingOrder1403() {
        int[] nums = {10, 1, 9, 2, 3, 8, 7};
        Arrays.sort(nums);
        int iSum = 0, iSumMax = 0;
        ArrayList<Integer> aLists = new ArrayList();
        for (int num : nums) {
            iSum += num;
        }
        for (int i = nums.length - 1; i > -1; i--) {
            if (iSumMax <= iSum) {
                iSumMax += nums[i];
                iSum -= nums[i];
                aLists.add(nums[i]);
            }
        }
        System.out.println(aLists);
    }
}
