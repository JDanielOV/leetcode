/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;
import java.math.BigDecimal;
import java.math.RoundingMode;
/**
 *
 * @author Daniel Ochoa
 */
public class MeanofArrayAfterRemovingSomeElements1619 {

    public void MeanofArrayAfterRemovingSomeElements1619() {
        int[] arr = {6, 0, 7, 0, 7, 5, 7, 8, 3, 4, 0, 7, 8, 1, 6, 8, 1, 1, 2, 4, 8, 1, 9, 5, 4, 3, 8, 5, 10, 8, 6, 6, 1, 0, 6, 10, 8, 2, 3, 4};
        Arrays.sort(arr);
        int iProm = (int) (arr.length * 0.05);
        double iSum = 0;
        System.out.println(iProm);
        for (int i = iProm; i < arr.length - iProm; i++) {
            iSum += arr[i];
        }
        System.out.println(BigDecimal.valueOf(iSum / (arr.length - (iProm * 2))).setScale(5,RoundingMode.CEILING).doubleValue());
    }
}
