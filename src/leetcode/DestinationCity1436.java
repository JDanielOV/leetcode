package leetcode;

import java.util.*;

public class DestinationCity1436 {
    public static void main(String[] args) {
        DestinationCity1436 v = new DestinationCity1436();
        List<List<String>> l = Arrays.asList(Arrays.asList("London", "New York"), Arrays.asList("New York", "Lima"));
        System.out.println(v.destCity(l));
    }

    public String destCity(List<List<String>> paths) {
        Map<String, String> mCiudades = new HashMap<>();
        String sDestino = "";
        for (List<String> lCiudad : paths) {
            if (!mCiudades.containsKey(lCiudad.get(0))) {
                mCiudades.put(lCiudad.get(0), lCiudad.get(1));
            }
        }

        for (String ciudad : mCiudades.values()
        ) {
            if (!mCiudades.containsKey(ciudad)) {
                sDestino = ciudad;
            }
        }
        return sDestino;
    }
}
