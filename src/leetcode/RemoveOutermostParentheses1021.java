/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Stack;
import java.util.ArrayList;

public class RemoveOutermostParentheses1021 {

    public void RemoveOutermostParentheses1021() {
        String S = "";
        ArrayList<Integer> aPositions = new ArrayList<>();
        Stack<Parentheses> sParents = new Stack<>();
        StringBuilder sReturn = new StringBuilder(S);
        for (int i = 0; i < S.length(); i++) {
            if (sParents.size() == 1 && S.charAt(i) == ')') {
                aPositions.add(sParents.pop().getiPosition());
                aPositions.add(i);
            } else if (sParents.size() > 0 && sParents.peek().getcValue() == '(' && S.charAt(i) == ')') {
                sParents.pop();
            } else {
                sParents.add(new Parentheses(S.charAt(i), i));
            }
        }
        for (int i = aPositions.size() - 1; i > -1; i--) {
//            System.out.println(aPositions.get(i));
            sReturn.deleteCharAt(aPositions.get(i));
        }
        System.out.println(sReturn.toString());
    }

    public class Parentheses {

        char cValue;
        int iPosition;

        public Parentheses(char cValue, int iPosition) {
            this.cValue = cValue;
            this.iPosition = iPosition;
        }

        public char getcValue() {
            return cValue;
        }

        public int getiPosition() {
            return iPosition;
        }

    }
}
