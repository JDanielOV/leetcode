/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class ToeplitzMatrix766 {
//{1, 2, 3, 4},
//            {5, 1, 2, 3},
//            {9, 5, 1, 2}

    public void ToeplitzMatrix766() {
        int[][] matrix = { //            {2, 3, 4},
            //            {1, 2, 3},
            //            {5, 1, 2},
            //            {1, 5, 9}
            //                                {1, 2, 3, 4},
            //                                {5, 1, 2, 3},
            //                                {9, 5, 1, 2}
            //            {1, 2, 3},
            //            {4, 5, 6},
            //            {7, 8, 9}
            {11, 74, 7, 93},
            {40, 11, 74, 7}
        };
        int iCoumnas = matrix[0].length - 1;
        int iFilas = matrix.length - 1;
        int iDiago = 0;
        while (iDiago < matrix[0].length - 1) {
            int i = iFilas;
            int j = iCoumnas;
            while (i != -1 && j != -1) {
                System.out.println(i + " " + j + " " + matrix[i][j]);
                i--;
                j--;
            }
            iCoumnas--;
            iDiago++;
        }
        iCoumnas = matrix[0].length - 1;
        iFilas = matrix.length - 2;
        iDiago = 0;
        while (iDiago < matrix[0].length - 1) {
            int i = iFilas;
            int j = iCoumnas;
            while (i != -1 && j != -1) {
                System.out.println(i + " " + j + " " + matrix[i][j]);
                i--;
                j--;
            }
            iFilas--;
            iDiago++;
        }
    }
}
