package leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class SumOfSubarrayRanges2104 {
    public long subArrayRanges(int[] nums) {
        long lSumTotal = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j <= nums.length; j++) {
                /*aListaNumeros.add(nums[j]);
                aListaNumeros.sort(Integer::compareTo);
                int iValorMinimo = aListaNumeros.get(0);
                int iValorMaximo = aListaNumeros.get(aListaNumeros.size() - 1);
                lSumTotal += (iValorMaximo - iValorMinimo);*/
                // Arrays.copyOfRange(nums, i, j);
                /*for (int k :
                        Arrays.copyOfRange(nums, i, j)) {
                    System.out.print(k + " ");
                }
                System.out.println("#");*/
                int[] iValores = Arrays.copyOfRange(nums, i, j);
                Arrays.sort(iValores);
                int iValorMinimo = iValores[0];
                int iValorMaximo = iValores[iValores.length - 1];
                lSumTotal += (iValorMaximo - iValorMinimo);
            }
        }
        return lSumTotal;
    }

    public static void main(String[] args) {
        SumOfSubarrayRanges2104 v = new SumOfSubarrayRanges2104();
        int[] i = {4, -2, -3, 4, 1};
        System.out.println(v.subArrayRanges(i));
    /*    i = new int[]{1, 3, 3};
        System.out.println(v.subArrayRanges(i));
        i = new int[]{1, 2, 3};
        System.out.println(v.subArrayRanges(i));*/

    }
}
