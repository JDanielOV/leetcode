package leetcode;

import java.util.TreeMap;
import java.util.HashSet;

public class RingsAndRods2103 {
    public int countPoints(String rings) {
        TreeMap<Character, HashSet<Character>> tRoads = new TreeMap<>();
        HashSet<Character> sValores;
        int iCantidadVarillasCompletas = 0;
        for (int i = 1; i < rings.length(); i += 2) {
            Character cValor = rings.charAt(i);
            if (tRoads.containsKey(cValor)) {
                sValores = tRoads.get(cValor);

            } else {
                sValores = new HashSet<>();
            }
            Character cNuevoValor = rings.charAt(i - 1);
            sValores.add(cNuevoValor);
            tRoads.put(cValor, sValores);
        }
        for (HashSet<Character> hValores :
                tRoads.values()) {
            if (hValores.size() == 3)
                iCantidadVarillasCompletas++;
        }
        return iCantidadVarillasCompletas;
    }

    public static void main(String[] args) {
        RingsAndRods2103 v = new RingsAndRods2103();
        System.out.println(v.countPoints("B0B6G0R6R0R6G9"));
        System.out.println(v.countPoints("B0R0G0R9R0B0G0"));
        System.out.println(v.countPoints("G4"));
    }
}
