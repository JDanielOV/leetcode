package leetcode;

import java.util.Arrays;

public class MaximumProductDifferenceBetweenTwoPairs1913 {

    public void maxProductDifference() {
        int[] nums = {5, 6, 2, 7, 4};
        Arrays.sort(nums);
        int iResult = (nums[nums.length - 1] * nums[nums.length - 2]) - (nums[0] * nums[1]);
        System.out.println("iResult = " + iResult);
    }

    public static void main(String[] args) {
        MaximumProductDifferenceBetweenTwoPairs1913 v = new MaximumProductDifferenceBetweenTwoPairs1913();
        v.maxProductDifference();
    }
}
