/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;

/**
 *
 * @author Daniel Ochoa
 */
public class NextGreaterElementI496 {

    public void NextGreaterElementI496() {
        int[] nums1 = {4, 1, 2};
        int[] nums2 = {1, 3, 4, 2};
        int iCounter = 0;
        for (int i : nums1) {
            StringBuilder st = new StringBuilder(Arrays.toString(nums2));
            st.deleteCharAt(0).deleteCharAt(st.length() - 1).insert(0, ' ');
            String[] sArr = st.toString().split(",");
            int iPosition = Arrays.asList(sArr).indexOf(" " + Integer.toString(i));
            int[] iArrAux = Arrays.copyOfRange(nums2, iPosition + 1, nums2.length);
            boolean bControl = true;
            for (Object object : iArrAux) {
                if (Integer.parseInt(object.toString()) > i) {
                    bControl = false;
                    nums1[iCounter] = Integer.parseInt(object.toString());
                    break;
                }
            }
            if (bControl) {
                nums1[iCounter] = -1;
            }
            iCounter++;
        }
        for (int i : nums1) {
            System.out.println(i);
        }
    }
}
