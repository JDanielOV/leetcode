package leetcode;

public class CheckIfTheSentenceIsPangram1832 {

    public boolean checkIfPangram() {
        String sentence = "thequickbrownfoxjumpsoverthelazydog";
        for (char i = 'a'; i <= 'z'; i++) {
            if (!sentence.contains("" + i)) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        CheckIfTheSentenceIsPangram1832 v = new CheckIfTheSentenceIsPangram1832();
        System.out.println(v.checkIfPangram());
    }
}
