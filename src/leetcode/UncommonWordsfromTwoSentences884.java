/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.util.List;

/**
 * "s z z z s" "s z ejt"
 *
 * @author Daniel Ochoa
 */
public class UncommonWordsfromTwoSentences884 {

    public void UncommonWordsfromTwoSentences884() {
        String A = "s z z z s", B = "s z ejt";
        List<String> lWordsA = filterUnique(listFrecuency(A));
        List<String> lWordsB = filterUnique(listFrecuency(B));
        List<String> aResult = search(lWordsA, B);
        aResult.addAll(search(lWordsB, A));
        for (String string : aResult) {
//            System.out.println(string);
        }
    }

    public Map<String, Integer> listFrecuency(String sValue) {
        Map<String, Integer> mWords = new HashMap();
        for (String string : sValue.split(" ")) {
            System.out.println("#" + string + "#");
            if (mWords.containsKey(string)) {
                mWords.put(string, mWords.get(string) + 1);
            } else {
                mWords.put(string, 1);
            }
        }
        return mWords;
    }

    public List<String> filterUnique(Map<String, Integer> mValues) {
        List<String> lReturn = new ArrayList();
        for (Map.Entry<String, Integer> entry : mValues.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            if (value == 1) {
                lReturn.add(key);
            }
        }
        return lReturn;
    }

    public List<String> search(List<String> A, String sValue) {
        List<String> aReturn = new ArrayList();
        List<String> B = Arrays.asList(sValue.split(" "));
        for (String string : A) {
            if (!B.contains(string)) {
                aReturn.add(string);
            }
        }
        return aReturn;
    }
}
