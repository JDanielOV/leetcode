/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.ArrayList;

/**
 *
 * @author Daniel Ochoa
 */
public class FizzBuzz412 {

    public void FizzBuzz412() {
        int n = 3;
//        String[] sNumbers = new String[n];
        ArrayList<String> aNumbers = new ArrayList();
        for (int i = 1; i <= n; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                aNumbers.add("FizzBuzz");
            } else if (i % 3 == 0) {
                aNumbers.add("Fizz");
            } else if (i % 5 == 0) {
                aNumbers.add("Buzz");
            } else {
                aNumbers.add(String.valueOf(i));
            }
        }
    }
}
