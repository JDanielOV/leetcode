package leetcode;

// 4 == 100
// 35== 100011

public class MinimumBitFlipstoConvertNumber2220 {
    public static void main(String[] args) {
        MinimumBitFlipstoConvertNumber2220 m = new MinimumBitFlipstoConvertNumber2220();
        System.out.println(m.minBitFlips(4, 35));
    }

    public int minBitFlips(int start, int goal) {
        String sInicio = (Integer.toString(start, 2));
        String sFinal = (Integer.toString(goal, 2));

        if (Math.min(sInicio.length(), sFinal.length()) < sFinal.length()) {
            sInicio = this.rellenar(sInicio, sFinal.length());
        } else if (Math.min(sInicio.length(), sFinal.length()) < sInicio.length()) {
            sFinal = this.rellenar(sFinal, sInicio.length());
        }
        System.out.println(sInicio);
        System.out.println(sFinal);
        System.out.println(Integer.parseInt(sInicio,2));
        System.out.println(Integer.parseInt(sFinal,2));

        int iContador = 0;
        for (int i = 0; i < sFinal.length(); i++) {
            if (sInicio.charAt(i) != sFinal.charAt(i)) {
                iContador++;
            }
        }

        return iContador;
    }

    public String rellenar(String sValor, int iLongitud) {
        while (sValor.length() < iLongitud) {
            sValor = "0" + sValor;
        }
        return sValor;
    }

}
