package leetcode;

import java.util.HashMap;
import java.util.ArrayList;

public class IntersectionOfTwoArraysII350 {
    public static void main(String[] args) {

    }

    public static int[] intersect(int[] nums1, int[] nums2) {
        HashMap<Integer, Integer> mNums1 = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> mNums2 = new HashMap<Integer, Integer>();
        ArrayList<Integer> aOutputs = new ArrayList<>();
        int[] iOutputs;
        for (int i : nums1) {
            if (mNums1.containsKey(i))
                mNums1.put(i, mNums1.get(i) + 1);
            else
                mNums1.put(i, 1);
        }
        for (int i : nums2) {
            if (mNums2.containsKey(i))
                mNums2.put(i, mNums2.get(i) + 1);
            else
                mNums2.put(i, 1);
        }
        for (HashMap.Entry<Integer, Integer> entry : mNums1.entrySet()) {
            Integer iKey = entry.getKey();
            if (mNums2.containsKey(iKey)) {
                for (int i = 0; i < Math.min(entry.getValue(), mNums2.get(iKey)); i++) {
                    aOutputs.add(iKey);
                }
            }
        }
        iOutputs = new int[aOutputs.size()];
        for (int i = 0; i < aOutputs.size(); i++) {
            iOutputs[i] = aOutputs.get(i);
        }
        return iOutputs;
    }
}
