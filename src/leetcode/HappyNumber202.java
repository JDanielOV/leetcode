package leetcode;

public class HappyNumber202 {
    public static void main(String[] args) {
        System.out.println(isHappy(19));
    }

    public static boolean isHappy(int n) {
        while (n > 9) {
            String sNumber = String.valueOf(n);
            char[] cDigits = sNumber.toCharArray();
            n = 0;
            for (char cValue : cDigits) {
                n += (Math.pow((cValue-48), 2));
            }
        }
        return n == 1 ||n==7;
    }
}
