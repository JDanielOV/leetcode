package leetcode;

import java.util.Arrays;

public class MaximumIceCreamBars1833 {

    public static int maxIceCream(int[] costs, int coins) {
        Arrays.sort(costs);
        int iSumCost = 0;
        int iTotalIces = 0;
        for (int i : costs) {
            iSumCost += i;
            if (iSumCost <= coins) {
                iTotalIces++;
            } else {
                break;
            }
        }
        return iTotalIces;
    }

    public static void main(String[] args) {
        int[] costs = {1,6,3,1,2,5};
        int coins = 20;
        System.out.println(maxIceCream(costs, coins));
    }
}
