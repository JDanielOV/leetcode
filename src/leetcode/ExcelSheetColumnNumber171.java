package leetcode;

public class ExcelSheetColumnNumber171 {
    public static void main(String[] args) {
        System.out.println(titleToNumber("ZXY"));
    }
    public static int titleToNumber(String columnTitle) {
        columnTitle=new StringBuilder(columnTitle).reverse().toString();
        int iOutput=columnTitle.charAt(0)-64;
        System.out.println("iOutput = " + iOutput);
        for (int i = 1; i < columnTitle.length(); i++) {
            iOutput+=(Math.pow((26),i)*(columnTitle.charAt(i)-64));
        }
        return iOutput;
    }
}
