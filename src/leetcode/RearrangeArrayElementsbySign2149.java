package leetcode;

public class RearrangeArrayElementsbySign2149 {
    public static void main(String[] args) {
        RearrangeArrayElementsbySign2149 r = new RearrangeArrayElementsbySign2149();
        for (int i :
                r.rearrangeArray(new int[]{1, 2, 3, -1, -2, -3})) {
            System.out.println(i);
        }
    }

    public int[] rearrangeArray(int[] nums) {
        int[] iPositivos = new int[nums.length / 2], iNegativos = new int[nums.length];
        int iContadorPositivo = 0, iContadorNegativo = 0;
        for (int i :
                nums) {
            if (i >= 0) {
                iPositivos[iContadorPositivo++] = i;
            } else {
                iNegativos[iContadorNegativo++] = i;
            }
        }
        int iContador = 0;
        for (int i = 0; i < iPositivos.length; i++) {
            nums[iContador++] = iPositivos[i];
            nums[iContador++] = iNegativos[i];
        }
        return nums;
    }
}
