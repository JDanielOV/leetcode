package leetcode;

import java.util.TreeMap;
import java.util.HashSet;

public class FindingtheUsersActiveMinutes1817 {
    public static void main(String[] args) {
        FindingtheUsersActiveMinutes1817 v = new FindingtheUsersActiveMinutes1817();
        for (int i :
                v.findingUsersActiveMinutes(new int[][]{
                        {0, 5}, {1, 2}, {0, 2}, {0, 5}, {1, 3}
                }, 5)) {
            System.out.println(i);
        }
    }

    public int[] findingUsersActiveMinutes(int[][] logs, int k) {
        int[] iResultados = new int[k];
        TreeMap<Integer, HashSet<Integer>> tmUsuarios = new TreeMap<>();
        for (int[] iUsuario : logs) {
            HashSet<Integer> hsMinutos;
            int iIdUsuario = iUsuario[0], iMinuto = iUsuario[1];
            if (!tmUsuarios.containsKey(iIdUsuario)) {
                hsMinutos = new HashSet<>();
            } else {
                hsMinutos = tmUsuarios.get(iIdUsuario);
            }
            hsMinutos.add(iMinuto);
            tmUsuarios.put(iIdUsuario, hsMinutos);
        }
        for (HashSet<Integer> hsMinutos :
                tmUsuarios.values()) {
            int iMinutosDiferentes = hsMinutos.size();
            iResultados[iMinutosDiferentes - 1]++;
        }
        return iResultados;
    }
}
