/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.time.LocalDate;
//import java.time.Month;

/**
 *
 * @author Daniel Ochoa
 */
public class DayoftheWeek1185 {

    public static void DayoftheWeek1185() {
        int day=31;
        int month=8;
        int year=2019;
        LocalDate dia = LocalDate.of(year, month, day);
        String sFecha=dia.getDayOfWeek().toString();
        StringBuilder sDia=new StringBuilder(sFecha.toLowerCase());
        sDia.setCharAt(0, (char)(sDia.charAt(0)-32));
        System.out.println("sDia = " + sDia);
    }
}
