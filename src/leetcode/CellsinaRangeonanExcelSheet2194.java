package leetcode;

import java.util.ArrayList;
import java.util.List;

public class CellsinaRangeonanExcelSheet2194 {
    public static void main(String[] args) {
        CellsinaRangeonanExcelSheet2194 c = new CellsinaRangeonanExcelSheet2194();
        System.out.println(c.cellsInRange("K1:L2"));
    }

    public List<String> cellsInRange(String s) {
        List<String> lResultados = new ArrayList<>();
        char cColumnaInicio = s.charAt(0);
        char cColumnaFinal = s.charAt(3);
//        System.out.println(cColumnaInicio + " " + cColumnaFinal);
        for (char i = cColumnaInicio; i <= cColumnaFinal; i++) {
            char cRenglonInicio = s.charAt(1);
            char cRenglonFinal = s.charAt(4);
//            System.out.println(cRenglonInicio + " " + cRenglonFinal);
            for (char j = cRenglonInicio; j <= cRenglonFinal; j++) {
                String sNuevaCelda = "" + i + j;
                lResultados.add(sNuevaCelda);
            }
        }
        return lResultados;
    }
}
