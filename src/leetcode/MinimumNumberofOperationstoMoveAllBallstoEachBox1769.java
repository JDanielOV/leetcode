package leetcode;

import java.util.ArrayList;

public class MinimumNumberofOperationstoMoveAllBallstoEachBox1769 {
    public static void main(String[] args) {
        MinimumNumberofOperationstoMoveAllBallstoEachBox1769 v = new MinimumNumberofOperationstoMoveAllBallstoEachBox1769();
        for (int i :
                v.minOperations("001011")) {
            System.out.print(i+" ");
        }
    }

    public int[] minOperations(String boxes) {
        int iLongitudCaja = boxes.length();
        int[] iMovimientos = new int[iLongitudCaja];

        for (int i = 0; i < iLongitudCaja; i++) {
            int iMovimientosCajaI = 0;
            for (int j = 0; j < iLongitudCaja; j++) {
                if (j != i) {
                    if (boxes.charAt(j) == '1')
                        iMovimientosCajaI += Math.abs(j - i);
                }
            }
            iMovimientos[i] = iMovimientosCajaI;
        }
        return iMovimientos;
    }
}
