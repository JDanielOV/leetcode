package leetcode;

public class WateringPlants2079 {
    public static void main(String[] args) {
        WateringPlants2079 v = new WateringPlants2079();
        v.wateringPlants(new int[]{2, 2, 3, 3}, 5);
    }

    public int wateringPlants(int[] plants, int capacity) {
        int iMovimientos = 0, iCapacidadActual = capacity;
        for (int i = 0; i < plants.length; i++) {
            int iAguaNecesaria = plants[i];
            if (iAguaNecesaria <= iCapacidadActual) {
                iCapacidadActual -= iAguaNecesaria;
                iMovimientos++;
            } else {
                iMovimientos += (i) + (i + 1);
                iCapacidadActual = capacity - iAguaNecesaria;
            }
            System.out.println(iMovimientos);
        }
        return iMovimientos;
    }
}
