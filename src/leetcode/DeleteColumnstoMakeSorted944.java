/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;

/**
 *
 * @author Daniel Ochoa
 */
public class DeleteColumnstoMakeSorted944 {

    public void DeleteColumnstoMakeSorted944() {
        String[] strs = {"abc", "bce", "cae"};
        int iCounterDeletes = 0;
        for (int i = 0; i < strs[0].length(); i++) {
            StringBuilder sTxt = new StringBuilder();
            for (String str : strs) {
                sTxt.append(str.charAt(i));
            }
            char[] cLetters = sTxt.toString().toCharArray();
            Arrays.sort(cLetters);
            if (!sTxt.toString().equals(String.valueOf(cLetters))) {
                iCounterDeletes++;
            }
        }
        System.out.println(iCounterDeletes);
    }
}
