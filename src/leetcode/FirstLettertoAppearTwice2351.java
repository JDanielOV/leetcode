package leetcode;

public class FirstLettertoAppearTwice2351 {
    public static void main(String[] args) {
        FirstLettertoAppearTwice2351 f = new FirstLettertoAppearTwice2351();
        System.out.println(f.repeatedCharacter("abaavaca"));
        System.out.println(f.repeatedCharacter("abacaabb"));
        System.out.println(f.repeatedCharacter("abccbaacz"));
        System.out.println(f.repeatedCharacter("adsdvbbasdgsvasdcasdad"));
    }

    public char repeatedCharacter(String s) {
        int iPosicionUno = -1;
        int iPosicionDos = -1;
        int Anterior = s.length()+1;
        char cValor = ' ';
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            iPosicionUno = s.indexOf(c, iPosicionUno + 1);
            iPosicionDos = s.indexOf(c, iPosicionUno + 1);
            if (Anterior > iPosicionDos && iPosicionDos > iPosicionUno) {
                cValor = c;
               Anterior = iPosicionDos;
            }
            //System.out.println(iPosicionUno + "--" + c + "--" + iPosicionDos);
        }
        return cValor;
    }
}
/**
 * "abccbaacz"
 * "abcdd"
 * "abacaabb"
 * "abaavaca"
 * "abccbaacz"
 * "aa"
 * "adsdvbbasdgsvasdcasdad"
 **/