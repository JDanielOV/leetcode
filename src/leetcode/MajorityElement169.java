package leetcode;

import java.util.Arrays;

public class MajorityElement169 {
    public static void main(String[] args) {
        System.out.println(majorityElement(new int[]{1, 2, 2, 2, 5, 2}));
    }

    public static int majorityElement(int[] nums) {
        Arrays.sort(nums);
        int iFrecuency = 1;
        int iMitad = nums.length % 2 == 0 ? nums.length / 2 : (nums.length / 2) + 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                iFrecuency++;
                if (iFrecuency == iMitad)
                    return nums[i];
            } else {
                if (iFrecuency == iMitad)
                    return nums[i - 1];
                iFrecuency = 1;
            }
        }
        return nums[nums.length - 1];
    }
}

