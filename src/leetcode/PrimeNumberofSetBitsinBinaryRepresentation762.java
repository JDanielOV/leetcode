/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class PrimeNumberofSetBitsinBinaryRepresentation762 {

    public void PrimeNumberofSetBitsinBinaryRepresentation762() {
        int L = 6, R = 10;
        int iCounter = 0;
        for (int i = L; i <= R; i++) {
            int iCountBites = Integer.bitCount(i);
            if (iCountBites == 2 || iCountBites == 3 || iCountBites == 5 || iCountBites == 7
                    || iCountBites == 11 || iCountBites == 13 || iCountBites == 17 || iCountBites == 19) {
                iCounter++;
            }
        }
        System.out.println();
    }
}
