/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.HashMap;
import java.util.Map;

public class MaximumNumberofBallsinaBox1742 {

    public void MaximumNumberofBallsinaBox1742() {
        int lowLimit = 19;
        int highLimit = 28;
        int iN = highLimit - lowLimit + 1;
        Map<Integer, Integer> mBoxes = new HashMap();
        for (int i = lowLimit; i < lowLimit + iN; i++) {
            int iBall = addBox(Integer.toString(i));
            if (mBoxes.containsKey(iBall)) {
                mBoxes.put(iBall, mBoxes.get(iBall) + 1);
            } else {
                mBoxes.put(iBall, 1);
            }
        }
        iN = 0;
        for (Integer value : mBoxes.values()) {
            if (value > iN) {
                iN = value;
            }
        }
        System.out.println(iN);
    }

    public int addBox(String sValue) {
        int iSum = 0;
        for (char c : sValue.toCharArray()) {
            iSum += Integer.parseInt("" + c);
        }
        return iSum;
    }
}
