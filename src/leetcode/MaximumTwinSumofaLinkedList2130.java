package leetcode;

import java.util.HashMap;
import java.util.ArrayList;

public class MaximumTwinSumofaLinkedList2130 {
    public static void main(String[] args) {
        MaximumTwinSumofaLinkedList2130 v = new MaximumTwinSumofaLinkedList2130();
        System.out.println(v.pairSum(new int[]{5, 4, 2, 1}));
    }

    public int pairSum(int[] head) {
        int iLongitudCabeza = head.length;
        ArrayList<Integer> aValores = new ArrayList<>();
        for (int i = 0; i < iLongitudCabeza; i++) {
            aValores.add(head[i]);
        }
        int iValorMaximo = 0;
        for (int i = 0; i < aValores.size() / 2; i++) {
            int iPar = aValores.get(i) + aValores.get(aValores.size() - 1 - i);
            iValorMaximo = Math.max(iValorMaximo, iPar);
        }
        return iValorMaximo;
    }
}
