/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class MergeStringsAlternately1768 {

    public static void MergeStringsAlternately1768() {
        String word1 = "ab", word2 = "pqrs";
        StringBuilder sReturn = new StringBuilder();
        int iSize = Math.min(word1.length(), word2.length());
        for (int i = 0; i < iSize; i++) {
            sReturn.append(word1.charAt(i));
            sReturn.append(word2.charAt(i));
        }
        if (iSize < word1.length()) {
            sReturn.append(word1.substring(iSize));
        }
        if (iSize < word2.length()) {
            sReturn.append(word2.substring(iSize));
        }
        System.out.println(sReturn);
    }
}
