/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Daniel Ochoa
 */
public class Shift2DGrid1260 {

    public void Shift2DGrid1260() {
        int[][] grid = {{3, 8, 1, 9}, {19, 7, 2, 5}, {4, 6, 11, 10}, {12, 0, 21, 13}};
        int k = 1;
        List<List<Integer>> aListNumbers = convert(grid);
        List<List<Integer>> aList = new ArrayList();
        if (k == 0) {
//            return aListNumbers;
        }
        for (int j = 0; j < k; j++) {
            aList = new ArrayList();
            for (List<Integer> aListNumber : aListNumbers) {
                aList.add(aListNumber.subList(0, aListNumber.size() - 1));
            }
            for (int i = 0; i < aListNumbers.size(); i++) {
                ArrayList<Integer> aTemp = new ArrayList();
                if (i == aListNumbers.size() - 1) {
                    aTemp.add(aListNumbers.get(i).get(aListNumbers.get(i).size() - 1));
                    aTemp.addAll(aList.get(0));
                    aList.set(0, aTemp);
                } else {
                    aTemp.add(aListNumbers.get(i).get(aListNumbers.get(i).size() - 1));
                    aTemp.addAll(aList.get(i + 1));
                    aList.set(i + 1, aTemp);
                }
            }
            aListNumbers = new ArrayList(aList);
        }
        for (List<Integer> arrayList : aList) {
            for (Integer integer : arrayList) {
                System.out.print(integer + " ");
            }
            System.out.println("");
        }
    }

    public List<List<Integer>> convert(int[][] iValues) {
        List<List<Integer>> aReturn = new ArrayList();
        for (int[] iValue : iValues) {
            ArrayList<Integer> aTemp = new ArrayList();
            for (int i : iValue) {
                aTemp.add(i);
            }
            aReturn.add(aTemp);
        }
        return aReturn;
    }
}
