package leetcode;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Stream;

public class CountNumberofPairsWithAbsoluteDifferenceK2006 {
    public static void main(String[] args) {
        CountNumberofPairsWithAbsoluteDifferenceK2006 v = new CountNumberofPairsWithAbsoluteDifferenceK2006();
        System.out.println(v.countKDifference(new int[]{1, 2, 2, 1}, 1));
    }

    public int countKDifference(int[] nums, int k) {
        int iContador = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i; j < nums.length; j++) {
                if (Math.abs(nums[i] - nums[j]) == k) {
                    iContador++;
                }
            }
        }
        return iContador;
    }
}
