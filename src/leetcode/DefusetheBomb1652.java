/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class DefusetheBomb1652 {

    public void DefusetheBomb1652() {
        int[] code = {5, 7, 1, 4};
        int k = 3;
        int[] iNumbers = new int[code.length];
        int[] iTempos = new int[2];
        for (int i = 0; i < code.length; i++) {
            iTempos = pos(code, iTempos[0], iTempos[1], k);
            iNumbers[i] = iTempos[0];
        }
        for (int iNumber : iNumbers) {
            System.out.print(iNumber + " ");
        }
    }

    public int[] pos(int[] iValues, int iSum, int iUltPos, int k) {
        int[] iReturn = new int[2];
        System.out.println(k + iUltPos);
        if (k + iUltPos > iValues.length-1) {
            iSum += iValues[(k + iUltPos) - iValues.length];
            iUltPos = (k + iUltPos) - iValues.length;
        } else {
            iSum += iValues[iUltPos + k];
            iUltPos = iUltPos + k;
        }
        System.out.println(iSum);
        System.out.println("----------------------");
        iSum -= iValues[iUltPos];
        iReturn[0] = iSum;
        iReturn[1] = iUltPos;
        return iReturn;
    }

}
