package leetcode;

import java.util.*;

public class DisplayTableofFoodOrdersinaRestaurant1418 {
    public static void main(String[] args) {
        DisplayTableofFoodOrdersinaRestaurant1418 v = new DisplayTableofFoodOrdersinaRestaurant1418();
        List<List<String>> lPedidos = new ArrayList<>();
        lPedidos.add(new ArrayList<String>(Arrays.asList("David", "3", "Ceviche")));
        lPedidos.add(new ArrayList<String>(Arrays.asList("Corina", "10", "Beef Burrito")));
        lPedidos.add(new ArrayList<String>(Arrays.asList("David", "3", "Fried Chicken")));
        lPedidos.add(new ArrayList<String>(Arrays.asList("Carla", "5", "Water")));
        lPedidos.add(new ArrayList<String>(Arrays.asList("Carla", "5", "Ceviche")));
        lPedidos.add(new ArrayList<String>(Arrays.asList("Rous", "3", "Ceviche")));
        v.displayTable(lPedidos);
    }

    public List<List<String>> displayTable(List<List<String>> orders) {
        TreeMap<Integer, TreeMap<String, Integer>> tmTablaPedidos = new TreeMap<>();
        HashSet<String> hsComidasEnElRestaurante = new HashSet<>();
        for (List l : orders) {
            Integer iNumeroMesa = Integer.valueOf(l.get(1).toString());
            String sComida = l.get(2).toString();
            hsComidasEnElRestaurante.add(sComida);
            if (!tmTablaPedidos.containsKey(iNumeroMesa)) {
                TreeMap<String, Integer> tmListaMesa = new TreeMap<>();
                tmListaMesa.put(sComida, 1);
                tmTablaPedidos.put(iNumeroMesa, tmListaMesa);
            } else {
                TreeMap<String, Integer> tmListaMesa = tmTablaPedidos.get(iNumeroMesa);
                if (!tmListaMesa.containsKey(sComida)) {
                    tmListaMesa.put(sComida, 1);
                } else {
                    tmListaMesa.put(sComida, tmListaMesa.get(sComida) + 1);
                }
            }
        }
       /* for (Map.Entry<Integer,TreeMap<String,Integer>> t: tmTablaPedidos.entrySet()) {
            System.out.println(t.getKey()+" "+t.getValue());
        }*/
        ArrayList<String> alMiListaDeComidas = new ArrayList<String>(hsComidasEnElRestaurante);
        alMiListaDeComidas.sort(String::compareTo);
        alMiListaDeComidas.add(0, "Table");
/*        for (String s:alMiListaDeComidas) {
            System.out.println(s);
        }*/
        orders = new ArrayList<>();
        orders.add(alMiListaDeComidas);
        alMiListaDeComidas.remove(0);
  /*      for (Map.Entry<Integer,TreeMap<String,Integer>> t: tmTablaPedidos.entrySet()) {
            System.out.println(t.getKey()+" "+t.getValue());
        }*/
        for (Map.Entry<Integer, TreeMap<String, Integer>> t : tmTablaPedidos.entrySet()) {
            ArrayList<String> alOrden = new ArrayList<>();
            alOrden.add(String.valueOf(t.getKey()));
            for (String s : alMiListaDeComidas) {
                if (t.getValue().containsKey(s)) {
                    alOrden.add(String.valueOf(t.getValue().get(s)));
                } else {
                    alOrden.add(String.valueOf(0));
                }
            }
            orders.add(alOrden);
        }
        orders.get(0).add(0, "Table");
/*        for (List<String> l : orders
        ) {
            System.out.println(l);
        }*/
        return orders;
    }
}
