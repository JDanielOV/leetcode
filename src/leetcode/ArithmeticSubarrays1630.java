package leetcode;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class ArithmeticSubarrays1630 {
    public static void main(String[] args) {
        ArithmeticSubarrays1630 v = new ArithmeticSubarrays1630();
        int[] nums = {-12,-9,-3,-12,-6,15,20,-25,-20,-15,-10};
        int[] l = {8};
        int[] r = {9};
        v.checkArithmeticSubarrays(nums, l, r);
    }

    public List<Boolean> checkArithmeticSubarrays(int[] nums, int[] l, int[] r) {
        List<Boolean> lResults = new ArrayList<Boolean>();
        for (int i = 0; i < l.length; i++) {
            lResults.add(checkArithmetic(nums, l[i], r[i]));
        }
        return lResults;
    }

    public boolean checkArithmetic(int nums[], int l, int r) {
        if (!(r - l >= 2)) {
            return false;
        }
        int[] iRanges = Arrays.copyOfRange(nums, l, r + 1);
        Arrays.sort(iRanges);
        int iValue = iRanges[1] - iRanges[0];
        for (int i = 2; i < iRanges.length; i++) {
            int iVal = iRanges[i] - iRanges[i - 1];
            if (iValue != iVal)
                return false;
        }
        return true;
    }
}
