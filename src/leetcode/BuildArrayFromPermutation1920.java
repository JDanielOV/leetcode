package leetcode;

public class BuildArrayFromPermutation1920 {

    public void buildArray() {
        int[] nums = {5, 0, 1, 2, 3, 4};
        int[] iResults = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            iResults[i] = nums[nums[i]];
        }
        for (int i : iResults) {
            System.out.print(i + " ");
        }
    }

    public static void main(String[] args) {
        BuildArrayFromPermutation1920 v = new BuildArrayFromPermutation1920();
        v.buildArray();
    }
}
