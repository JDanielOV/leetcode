package leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

public class Volumen1 {

    public void ValidPalindrome() {
        String sCadena = "A man, a plan, a canal: Panama";
        Values vArr[] = new Values[4];
        for (int i = 0; i < sCadena.length(); i++) {
            Values vElement = new Values();
            vElement.setiPosition(i);
            vElement.setiValue((int) (sCadena.charAt(i)));
            vArr[i] = vElement;
        }
        for (int i = 0; i < vArr.length; i++) {
            System.out.println((char) (vArr[i].getiValue()) + "-" + vArr[i].getiPosition());
        }
    }

    public class Values {

        private int iValue;
        private int iPosition;

        public int getiValue() {
            return iValue;
        }

        public void setiValue(int iValue) {
            this.iValue = iValue;
        }

        public int getiPosition() {
            return iPosition;
        }

        public void setiPosition(int iPosition) {
            this.iPosition = iPosition;
        }
//        public Values(int iValues,int iPosition)
//        {
//            this.iValue=iValues;
//            this.iPosition=iPosition;
//        }
    }

    public void LengthofLastWord() {
        String s = "Hello World";
        String sArr[] = s.split(" ");
        if (sArr.length == 0) {
            System.out.println(0);
        } else {
            System.out.println(sArr[sArr.length - 1].length());
        }
    }

    public void DecryptStringfromAlphabettoIntegerMapping() {
        String s = "12345678910#11#12#13#14#15#16#17#18#19#20#21#22#23#24#25#26#";
        StringTokenizer sToken = new StringTokenizer(s, "#");
        Vector<String> vSplit = new Vector<String>();
        String sTemporal;
        while (sToken.hasMoreTokens()) {
            sTemporal = sToken.nextToken();
            vSplit.add(sTemporal);

        }
        sTemporal = "";
        if (s.charAt(s.length() - 1) != '#') {
            s = vSplit.get(vSplit.size() - 1);
            vSplit.remove(vSplit.size() - 1);
            if (s.length() == 1) {
                sTemporal = (returnSizeOne(s));
            } else if (s.length() >= 2) {
                sTemporal = (returnEnd(s));
            }
        }
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < vSplit.size(); i++) {
            s = vSplit.get(i);
            if (s.length() == 2) {
                sBuffer.append(returnSizeTwo(s));
            } else if (s.length() == 1) {
                sBuffer.append(returnSizeOne(s));
            } else if (s.length() > 2) {
                sBuffer.append(returnSizeMoreTwo(s));
            }
        }
        sBuffer.append(sTemporal);
        System.out.println(sBuffer.toString());
    }

    public String returnSizeTwo(String sValue) {
        char iValue = (char) (Integer.parseInt(sValue) + 96);
        StringBuffer sConversion = new StringBuffer();
        sConversion.append((char) (iValue));
        return (sConversion.toString());
    }

    public String returnSizeOne(String sValue) {
        char iValue = (char) (Integer.parseInt(sValue) + 96);
        StringBuffer sConversion = new StringBuffer();
        sConversion.append((char) (iValue));
        return (sConversion.toString());
    }

    public String returnSizeMoreTwo(String sValue) {
        StringBuffer sReturn = new StringBuffer();
        for (int i = 0; i < sValue.length() - 2; i++) {
            StringBuffer sTemporal = new StringBuffer();
            sTemporal.append(sValue.charAt(i));
            sReturn.append(returnSizeOne(sTemporal.toString()));
        }
        StringBuffer sTwoEnds = new StringBuffer(sValue.substring(sValue.length() - 2));
        sReturn.append(returnSizeTwo(sTwoEnds.toString()));
        return sReturn.toString();
    }

    public String returnEnd(String sValue) {
        StringBuffer sReturn = new StringBuffer();
        for (int i = 0; i < sValue.length(); i++) {
            StringBuffer sTemporal = new StringBuffer();
            sTemporal.append(sValue.charAt(i));
            sReturn.append(returnSizeOne(sTemporal.toString()));
        }
        return sReturn.toString();
    }

    public void UniqueEmailAddresses() {
        String[] emails = {"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"};
        StringBuffer sReturn;
        Vector<String> vDifferent = new Vector();
        for (int i = 0; i < emails.length; i++) {
            sReturn = new StringBuffer();
            String sDominio = emails[i].substring(0, emails[i].indexOf('@'));
            sReturn.append(characterPoints(sDominio));
            sReturn.append(emails[i].substring(emails[i].indexOf('@')));
            vDifferent = verifyExistence(vDifferent, sReturn.toString());
        }
        System.out.println(vDifferent.size());
    }

    public String characterPlus(String sValue) {
        if (sValue.indexOf('+') == -1) {
            return sValue;
        }
        String sReturn = sValue.substring(0, sValue.indexOf('+'));
        return sReturn;
    }

    public String characterPoints(String sValue) {
        StringBuffer sReturn = new StringBuffer(characterPlus(sValue));
        for (int i = 0; i < sReturn.length(); i++) {
            if (sReturn.charAt(i) == '.') {
                sReturn.replace(i, i + 1, "");
                i = i - 1;
            }
        }
        return sReturn.toString();
    }

    public Vector<String> verifyExistence(Vector<String> vDifferent, String sValue) {
        if (vDifferent.indexOf(sValue) == -1) {
            vDifferent.add(sValue);
            return vDifferent;
        } else {
            return vDifferent;
        }
    }

    public void MaximumNumberofBalloons() {
        String text = "leetcode";
//        balloon
        Map<Integer, Integer> mMapeo = new HashMap<Integer, Integer>();
        int iContO = 0, iContL = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == 'b' || text.charAt(i) == 'a' || text.charAt(i) == 'n') {
                if (mMapeo.get((int) (text.charAt(i))) == null) {
                    mMapeo.put((int) (text.charAt(i)), 1);
                } else {
                    mMapeo.put((int) (text.charAt(i)), (mMapeo.get((int) (text.charAt(i)))) + 1);
                }
            }
            if ((text.charAt(i)) == 'l') {
                iContL++;
            }
            if (text.charAt(i) == (int) 'o') {
                iContO++;
            }
        }
        System.out.println(iContO / 2);
        System.out.println(iContL / 2);
        System.out.println(returnContMinimum(mMapeo));
        if (mMapeo.size() == 3) {
            System.out.println("res" + returnMinimumValue(iContO / 2, iContL / 2, returnContMinimum(mMapeo)));
        } else {
            System.out.println(0);
        }

    }

    public int returnContMinimum(Map<Integer, Integer> mMapeo) {
        int iMenor = 999999;
        for (Map.Entry<Integer, Integer> entry : mMapeo.entrySet()) {
            Integer value = entry.getValue();
            if (iMenor > value) {
                iMenor = value;
            }
        }
        return iMenor;
    }

    public int returnMinimumValue(int iContO, int iContL, int iMapeo) {
        int iArr[] = {iContL, iContO, iMapeo};

        for (int i = 0; i < iArr.length - 1; i++) {
            for (int j = 0; j < iArr.length - i - 1; j++) {
                if (iArr[j + 1] < iArr[j]) {
                    int aux = iArr[j + 1];
                    iArr[j + 1] = iArr[j];
                    iArr[j] = aux;
                }
            }
        }
        return iArr[0];
    }

}
