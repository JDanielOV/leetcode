/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.ArrayList;

/**
 *
 * @author Daniel Ochoa
 */
public class LastStoneWeight1046 {

    public void LastStoneWeight1046() {
        int[] stones = {2, 7, 4, 1, 8, 1};
        ArrayList<Integer> aListNumbers = new ArrayList();
        for (int stone : stones) {
            aListNumbers.add(stone);
        }
        System.out.println(romper(aListNumbers));
    }

    public int romper(ArrayList<Integer> aValues) {
        aValues.sort((x, y) -> x - y);
        for (Integer aValue : aValues) {
            System.out.print(aValue+" ");
        }
        System.out.println("");
        if (aValues.size() == 0) {
            return 0;
        } else if (aValues.size() == 1) {
            return aValues.get(0);
        }
        int X = aValues.get(aValues.size() - 1);
        int Y = aValues.get(aValues.size() - 2);
        if (X == Y) {
            aValues.remove(aValues.size() - 1);
            aValues.remove(aValues.size() - 1);
        } else {
            aValues.set(aValues.size() - 1, X - Y);
            aValues.remove(aValues.size() - 2);
        }
        return romper(aValues);
    }
}
