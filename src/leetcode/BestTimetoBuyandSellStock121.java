package leetcode;

import java.util.Arrays;

public class BestTimetoBuyandSellStock121 {
    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};
        BestTimetoBuyandSellStock121 b = new BestTimetoBuyandSellStock121();
        System.out.println(b.maxProfit(prices));
    }

    public int maxProfit(int[] prices) {
        int iMaxProfit = 0;
        for (int i = 0; i < prices.length; i++) {
            int iValue = this.iMaxValue(prices, i) - prices[i];
            if (iValue > iMaxProfit)
                iMaxProfit = iValue;
        }
        return iMaxProfit;
    }

    public int iMaxValue(int[] prices, int iIndex) {
        int[] iPrices = Arrays.copyOfRange(prices, iIndex,prices.length);
        Arrays.sort(iPrices);
        return iPrices[iPrices.length-1];
    }
}
