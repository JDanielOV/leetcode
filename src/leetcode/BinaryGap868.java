/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class BinaryGap868 {

    public static void BinaryGap868() {
        int n = 22;
        String sBinary = Integer.toBinaryString(n);
        int iDistanciaMax = 0;
        int iPositionInicial = sBinary.indexOf('1');
        int iPositionAdj = sBinary.indexOf('1', iPositionInicial+1);
        while (iPositionInicial != -1) {
            if (iPositionAdj != -1) {
                int iDiferencia = iPositionAdj - iPositionInicial;
                System.out.println(iPositionInicial+" "+iPositionAdj);
                if (iDiferencia > iDistanciaMax) {
                    iDistanciaMax = iDiferencia;
                }
            }
            iPositionInicial = iPositionAdj;
            iPositionAdj = sBinary.indexOf('1', iPositionInicial+1);
        }
        System.out.println("iDistanciaMax = " + iDistanciaMax);
    }
}
