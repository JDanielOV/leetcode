package leetcode;

public class RemoveAllOccurrencesOfASubstring1910 {
    public static void main(String[] args) {
        RemoveAllOccurrencesOfASubstring1910 v = new RemoveAllOccurrencesOfASubstring1910();
        System.out.println(v.removeOccurrences("daabcbaabcbc", "abc"));
    }

    public String removeOccurrences(String s, String part) {
        StringBuilder sbTextReform = new StringBuilder(s);
        int iPosition = sbTextReform.indexOf(part);
        while (iPosition != -1) {
            sbTextReform.delete(iPosition, iPosition + part.length());
            iPosition = sbTextReform.indexOf(part);
        }
        return sbTextReform.toString();
    }
}
