/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class IslandPerimeter463 {

    public void IslandPerimeter463() {
        int[][] grid = {
            {0, 1, 0, 0},
            {1, 1, 1, 0},
            {0, 1, 0, 0},
            {1, 1, 0, 0}};
        int iPeremitro = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 1) {
                    iPeremitro += perimetro(grid, i, j);
                }
            }
        }
        System.out.println(iPeremitro);
    }

    public int perimetro(int[][] grid, int i, int j) {
        int iPerimetro = 4;
        if (i == 0) {
            if (j == 0) {
                if (grid.length - 1 > i /*&& grid[0].length - 1 > j*/ && grid[i + 1][j] == 1) {
                    iPerimetro--;
                }
                if (/*grid.length - 1 > i &&*/grid[0].length - 1 > j && grid[i][j + 1] == 1) {
                    iPerimetro--;
                }

            } else if (j == grid[0].length - 1) {
                if (grid.length - 1 > i /*&& grid[0].length - 1 > j*/ && grid[i + 1][j] == 1) {
                    iPerimetro--;
                }
                if (grid[i][j - 1] == 1) {
                    iPerimetro--;
                }
            } else {
                if (/*grid.length - 1 > i &&*/grid[0].length - 1 > j && grid[i][j + 1] == 1) {
                    iPerimetro--;
                }
                if (grid[i][j - 1] == 1) {
                    iPerimetro--;
                }
                if (grid.length - 1 > i /*&& grid[0].length - 1 > j*/ && grid[i + 1][j] == 1) {
                    iPerimetro--;
                }
            }
        } else if (i == grid.length - 1) {
            if (j == 0) {
                if (grid[i - 1][j] == 1) {
                    iPerimetro--;
                }
                if (/*grid.length - 1 > i &&*/grid[0].length - 1 > j && grid[i][j + 1] == 1) {
                    iPerimetro--;
                }
            } else if (j == grid[0].length - 1) {
                if (grid[i - 1][j] == 1) {
                    iPerimetro--;
                }
                if (grid[i][j - 1] == 1) {
                    iPerimetro--;
                }
            } else {
                if (/*grid.length - 1 > i &&*/grid[0].length - 1 > j && grid[i][j + 1] == 1) {
                    iPerimetro--;
                }
                if (grid[i][j - 1] == 1) {
                    iPerimetro--;
                }
                if (grid[i - 1][j] == 1) {
                    iPerimetro--;
                }
            }
        } else {
            if (j == 0) {
                if (grid[i - 1][j] == 1) {
                    iPerimetro--;
                }
                if (grid[i + 1][j] == 1) {
                    iPerimetro--;
                }
                if (/*grid.length - 1 > i &&*/grid[0].length - 1 > j && grid[i][j + 1] == 1) {
                    iPerimetro--;
                }
            } else if (j == grid[0].length - 1) {
                if (grid[i - 1][j] == 1) {
                    iPerimetro--;
                }
                if (grid[i + 1][j] == 1) {
                    iPerimetro--;
                }
                if (grid[i][j - 1] == 1) {
                    iPerimetro--;
                }
            } else {
                if (grid[i - 1][j] == 1) {
                    iPerimetro--;
                }
                if (grid[i + 1][j] == 1) {
                    iPerimetro--;
                }
                if (grid[i][j - 1] == 1) {
                    iPerimetro--;
                }
                if (/*grid.length - 1 > i &&*/grid[0].length - 1 > j && grid[i][j + 1] == 1) {
                    iPerimetro--;
                }
            }
        }
        return iPerimetro;
    }
}
