/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class NumberofLinesToWriteString806 {

    public void NumberofLinesToWriteString806() {
        int[] widths = {10, 5, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
        String s = "ababbbbaabbbab";//|ababbbbaabbbab 95|ababbbbbababab 95|abaab
        int iLineas = 1, iSum = 0;
        int iCounter = 0;
        for (char c : s.toCharArray()) {
            int iValue = widths[c - 'a'];
            if (iValue + iSum <= 100) {
                iSum += iValue;
            } else {
                System.out.println(iCounter);
                iSum = iValue;
                iLineas++;
            }
            iCounter++;
        }
        widths = new int[2];
        widths[0] = iLineas;
        widths[1] = iSum;
        System.out.println(widths[0] + " " + widths[1]);
    }
}
