package leetcode;

public class PartitioningIntoMinimumNumberOfDeci_BinaryNumbers1689 {
    public static void main(String[] args) {
        PartitioningIntoMinimumNumberOfDeci_BinaryNumbers1689 v = new PartitioningIntoMinimumNumberOfDeci_BinaryNumbers1689();
        System.out.println(v.minPartitions("82734"));
        System.out.println(v.minPartitions("32"));
        System.out.println(v.minPartitions("27346209830709182346"));
    }

    public int minPartitions(String n) {
        for (char i = '9'; i >= '1'; i--) {
            if (n.contains(i + "")) {
                return Integer.parseInt(i + "");
            }
        }
        return 0;
    }
}
