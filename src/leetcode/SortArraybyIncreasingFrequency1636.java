/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Comparator;

public class SortArraybyIncreasingFrequency1636 {

    public void SortArraybyIncreasingFrequency1636() {
        int[] nums = {2, 3, 1, 3, 2};
        Map<Integer, Integer> mValues = new HashMap();
        ArrayList<value> aValues = new ArrayList();
        Comparator<value> sorterFrecuencia = (v1, v2) -> v1.getiFrecuencia() - v2.getiFrecuencia();
        Comparator<value> sorterValor = (v1, v2) -> v2.getiValue() - v1.getiValue();
        for (int num : nums) {
            if (mValues.containsKey(num)) {
                mValues.put(num, mValues.get(num) + 1);
            } else {
                mValues.put(num, 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : mValues.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            aValues.add(new value(key.intValue(), value.intValue()));
        }
        aValues.sort(sorterFrecuencia.thenComparing(sorterValor));
        int j = 0;
        for (value aValue : aValues) {
            for (int i = 0; i < aValue.getiFrecuencia(); i++) {
                nums[j] = aValue.getiValue();
                j++;
            }
        }
    }

    public class value {

        private int iValue;
        private int iFrecuencia;

        public value(int iValue, int iFrecuencia) {
            this.iValue = iValue;
            this.iFrecuencia = iFrecuencia;
        }

        public int getiValue() {
            return iValue;
        }

        public void setiValue(int iValue) {
            this.iValue = iValue;
        }

        public int getiFrecuencia() {
            return iFrecuencia;
        }

        public void setiFrecuencia(int iFrecuencia) {
            this.iFrecuencia = iFrecuencia;
        }

    }
}
