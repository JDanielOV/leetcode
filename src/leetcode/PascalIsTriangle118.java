package leetcode;

import java.util.ArrayList;
import java.util.Arrays;

public class PascalIsTriangle118 {
    public static void main(String[] args) {
        for (ArrayList<Integer> aList : generate(4)) {
            for (int i : aList) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
    }

    public static ArrayList<ArrayList<Integer>> generate(int numRows) {
        ArrayList<ArrayList<Integer>> aListOutputs = new ArrayList<>();
        aListOutputs.add(new ArrayList(Arrays.asList(new Integer[]{1})));
        if (numRows == 1)
            return aListOutputs;
        aListOutputs.add(new ArrayList(Arrays.asList(new Integer[]{1, 1})));
        if (numRows == 2)
            return aListOutputs;
        for (int i = 3; i <= numRows; i++) {
            ArrayList<Integer> aList = new ArrayList<>();
            aList.add(1);
            for (int j = 1; j <= aListOutputs.get(i - 2).size() - 1; j++) {
                aList.add(aListOutputs.get(i - 2).get(j - 1) + aListOutputs.get(i - 2).get(j));
            }
            aList.add(1);
            aListOutputs.add(aList);
        }
        return aListOutputs;
    }

}
