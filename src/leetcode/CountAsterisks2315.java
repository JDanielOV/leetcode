package leetcode;

import java.util.Arrays;
import java.util.StringTokenizer;

public class CountAsterisks2315 {
    public static void main(String[] args) {
        CountAsterisks2315 c = new CountAsterisks2315();
        System.out.println(c.countAsterisks("l|*e*et|c**o|*de|"));
        System.out.println(c.countAsterisks("iamprogrammer"));
        System.out.println(c.countAsterisks("yo|uar|e**|b|e***au|tifu|l"));
        System.out.println(c.countAsterisks("y****************ou|asdfdsfsd***************************fds|"));
        System.out.println(c.countAsterisks("|asdasd||asdasf|"));
        System.out.println(c.countAsterisks("|dfsdf|f**********"));
        System.out.println(c.countAsterisks("*****|sdfs**df|*******"));
        System.out.println(c.countAsterisks("**da**sfsdfg*sdfsg**"));
        System.out.println(c.countAsterisks("|**|*|*|**||***||"));
    }

    public int countAsterisks(String s) {
        String[] sValores = s.split("(\\|[\\w\\*]*\\|)+");
        return Arrays.stream(sValores)
                .map(sValor -> {
                    int iAstericos = 0;
                    for (char c :
                            sValor.toCharArray()) {
                        if (c == '*') {
                            iAstericos++;
                        }
                    }
                    return iAstericos;
                })
                .reduce(Integer::sum)
                .orElse(0);
    }
}
