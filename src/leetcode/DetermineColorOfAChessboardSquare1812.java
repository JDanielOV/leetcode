package leetcode;

public class DetermineColorOfAChessboardSquare1812 {
    public static void main(String[] args) {
        DetermineColorOfAChessboardSquare1812 v = new DetermineColorOfAChessboardSquare1812();
        System.out.println(v.squareIsWhite("a1"));
    }

    public boolean squareIsWhite(String coordinates) {
        boolean[] bFirstRow = {false, true, false, true, false, true, false, true};
        char cLetter = 'a';
        int iValue = Integer.parseInt(coordinates.charAt(1) + "");
        int iPos = coordinates.charAt(0) - cLetter;
        System.out.println("iValue = " + iValue);
        if (iValue % 2 == 0)
            return !bFirstRow[iPos];
        else
            return bFirstRow[iPos];
    }
}
