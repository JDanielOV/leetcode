package leetcode;

public class ValidPalindrome125 {
    public static void main(String[] args) {
        ValidPalindrome125 v=new ValidPalindrome125();
        System.out.println(v.isPalindrome("0P"));
    }
    public boolean isPalindrome(String s) {
        String[] sMyWords=s.split("([^A-Za-z0-9])+");
        StringBuilder sMyPalindrome=new StringBuilder();
        for (String S:sMyWords) {
            sMyPalindrome.append(S.toLowerCase());
        }
        System.out.println(sMyPalindrome);
        return sMyPalindrome.toString().equals(sMyPalindrome.reverse().toString());
    }
}
