package leetcode;

public class CountSortedVowelStrings1641 {
    public static void main(String[] args) {
        CountSortedVowelStrings1641 v = new CountSortedVowelStrings1641();
        System.out.println(v.countVowelStrings(3));
    }

    public int countVowelStrings(int n) {
        int[] iValues = {1, 1, 1, 1, 1};
        for (int i = 0; i < n; i++) {
            int iValue = 0;
            for (int j = 4; j > -1; j--) {
                iValue += iValues[j];
                iValues[j] = iValue;
            }
        }
        return iValues[0];
    }
}
