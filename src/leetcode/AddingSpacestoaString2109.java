package leetcode;

public class AddingSpacestoaString2109 {
    public static void main(String[] args) {
        AddingSpacestoaString2109 v = new AddingSpacestoaString2109();
        System.out.println(v.addSpaces("icodeinpython", new int[]{1, 5, 7, 9}));
    }

    public String addSpaces(String s, int[] spaces) {
        StringBuilder sResult = new StringBuilder(s);
        for (int i = spaces.length - 1; i > -1; i--) {
            sResult.insert(spaces[i], " ");
        }
        return sResult.toString();
    }
}
