package leetcode;

import java.util.Arrays;
import java.util.Optional;

public class FinalValueofVariableAfterPerformingOperations2011 {
    public static void main(String[] args) {
        FinalValueofVariableAfterPerformingOperations2011 v = new FinalValueofVariableAfterPerformingOperations2011();
        System.out.println(v.finalValueAfterOperations(new String[]{"++X","++X","X++"}));
    }

    public int finalValueAfterOperations(String[] operations) {
        return Arrays.stream(operations).map(s -> {
            if (s.equals("++X") || s.equals("X++")) {
                return 1;
            }
            return -1;
        }).reduce(0, Integer::sum);
    }
}
