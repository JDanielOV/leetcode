/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;

/**
 *
 * @author Daniel Ochoa
 */
public class ArrayPartitionI561 {

    public void ArrayPartitionI561() {
        int[] nums = {7, 10, 5, 6, 5, 15, 8, 12};
        Arrays.sort(nums);
        int iSum = 0;
        for (int i = 0; i < nums.length; i += 2) {
            iSum += Math.min(nums[i], nums[i]);
        }
        System.out.println(iSum);
    }
}
