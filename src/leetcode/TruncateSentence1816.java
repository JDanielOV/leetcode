package leetcode;

public class TruncateSentence1816 {
    public String truncateSentence(String s, int k) {
        String[] sWords = s.split(" ");
        StringBuilder sbTruncate = new StringBuilder();
        for (int i = 0; i < k; i++) {
            sbTruncate.append(sWords[i]);
            sbTruncate.append(" ");
        }
        sbTruncate.deleteCharAt(sbTruncate.length() - 1);
        return sbTruncate.toString();
    }

    public static void main(String[] args) {
        TruncateSentence1816 v = new TruncateSentence1816();
        System.out.println(v.truncateSentence("Hello how are you Contestant", 4) + "#");
    }
}
