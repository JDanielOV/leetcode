/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class LongestNiceSubstring1763 {

    public void LongestNiceSubstring1763() {
        String s = "YazaAay";
        String sTemporal = s.toLowerCase();
        String sResult = "";
        for (int i = 0; i < s.length(); i++) {
            int iPosition = sTemporal.indexOf(sTemporal.charAt(i), i + 1);
            while (iPosition != -1) {
                String sSub = sTemporal.substring(i, iPosition + 1);
                if (sResult.length() < sSub.length()) {
                    if (comprobar(s.substring(i, iPosition + 1))) {
                        sResult = s.substring(i, iPosition + 1);
                    }
                }
                iPosition = sTemporal.indexOf(sTemporal.charAt(i), iPosition + 1);
            }
        }
        System.out.println(sResult);
    }

    public boolean comprobar(String sValue) {
        for (char c : sValue.toCharArray()) {
            int iPosition = -1;
            if (Character.isLowerCase(c)) {
                iPosition = sValue.indexOf(c - 32);
            } else {
                iPosition = sValue.indexOf(c + 32);
            }
            if (iPosition == -1) {
                return false;
            }
        }
        return true;
    }

}
