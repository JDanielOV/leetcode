package leetcode;

public class SumofAbsoluteDifferencesinaSortedArray1685 {

    public void SumofAbsoluteDifferencesinaSortedArray1685() {
        int[] nums = {1, 2, 3, 3, 4, 5, 6, 7, 7, 7, 8, 9};
        int iResultArr[] = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                iResultArr[i] = iResultArr[i - 1];
                continue;
            }
            int iSumIndex = 0;
            for (int o : nums) {
                if (o == nums[i]) {
                    continue;
                } else {
                    iSumIndex += (Math.abs(nums[i] - o));
                }
            }
            iResultArr[i] = iSumIndex;;
        }
        for (int j : iResultArr) {
            System.out.print(j + " ");
        }
    }
}
