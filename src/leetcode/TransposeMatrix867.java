/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class TransposeMatrix867 {

    public void TransposeMatrix867() {
        int[][] matrix = {{1, 2}, {3, 4}, {5, 6}};
        int[][] numbers = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                numbers[j][i] = matrix[i][j];
            }
        }
        for (int[] number : numbers) {
            for (int i : number) {
                System.out.print(i + " ");
            }
            System.out.println("");
        }
    }
}
