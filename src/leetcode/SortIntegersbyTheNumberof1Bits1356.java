/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Daniel Ochoa
 */
public class SortIntegersbyTheNumberof1Bits1356 {

    public void SortIntegersbyTheNumberof1Bits1356() {
        int[] arr = {1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1};
        List<Integer> aList = Arrays.stream(arr).boxed().collect(Collectors.toList());
        Comparator<Integer> sorterOnes = (n1, n2) -> countOnes(n1) - countOnes(n2);
        Comparator<Integer> sorterNumbers = (n1, n2) -> (n1) - (n2);
        aList.sort(sorterOnes.thenComparing(sorterNumbers));
        arr = aList.stream().mapToInt(x -> x).toArray();
    }

    public int countOnes(int iValue) {
        int iCounterOnes = 0;
        char[] cNumbers = Integer.toBinaryString(iValue).toCharArray();
        for (char cNumber : cNumbers) {
            if (cNumber == '1') {
                iCounterOnes++;
            }
        }
        return iCounterOnes;
    }
}
