package leetcode;

public class MinimumTimeVisitingAllPoints1266 {
    public static void main(String[] args) {
        MinimumTimeVisitingAllPoints1266 c = new MinimumTimeVisitingAllPoints1266();
        System.out.println(c.minTimeToVisitAllPoints(new int[][]{{1, 1}, {3, 4}, {-1, 0}}));
    }

    /**
     * [1,1] -> [4,2]
     * X = 3
     * Y = 1
     * [[1,2],[4,2],[4,8],[-34,8]]
     * [[1,2],[4,2],[4,8],[-34,8],[-34,0],[5,0],[0,0],[0,13],[78,13]]
     **/

    public int minTimeToVisitAllPoints(int[][] points) {
        int x = points[0][0];
        int y = points[0][1];
        int iTiempo = 0;

        for (int i = 1; i < points.length; i++) {
            if (x == points[i][0]) {
                iTiempo += Math.abs(points[i][1] - y);
            } else if (y == points[i][1]) {
                iTiempo += Math.abs(points[i][0] - x);
            } else {
                int iDifX = Math.abs(points[i][0] - x);
                int iDifY = Math.abs(points[i][1] - y);

                iTiempo += Math.max(iDifX, iDifY);
            }
            x = points[i][0];
            y = points[i][1];
        }

        return iTiempo;
    }
}
