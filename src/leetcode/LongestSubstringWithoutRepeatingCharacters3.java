package leetcode;

public class LongestSubstringWithoutRepeatingCharacters3 {
    public int lengthOfLongestSubstring(String s) {
        int iMaxLong = 0;
        for (int i = 0; i < s.length(); i++) {
            iMaxLong = Math.max(iMaxLong, longitudPalabra(s.substring(i)));
        }
        return iMaxLong;
    }

    public int longitudPalabra(String s) {
        StringBuilder sbText = new StringBuilder("");
        for (int i = 0; i < s.length(); i++) {
            if (sbText.indexOf("" + s.charAt(i)) != -1) {
                return i;
            } else {
                sbText.append(s.charAt(i));
            }
        }
        return s.length();
    }

    public static void main(String[] args) {
        LongestSubstringWithoutRepeatingCharacters3 v = new LongestSubstringWithoutRepeatingCharacters3();
        System.out.println(v.lengthOfLongestSubstring(" "));
    }
}
