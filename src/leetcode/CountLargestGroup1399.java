/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Daniel Ochoa
 */
public class CountLargestGroup1399 {

    public void CountLargestGroup1399() {
        int n = 13;
        Map<Integer, Integer> mNumbers = new HashMap();
        int iMax = 1, iCounter = 0;
        for (int i = 1; i <= n; i++) {
            int iValue = sumaDeDigitos(i);
            if (mNumbers.containsKey(iValue)) {
                int iActualizarFrecuencia = mNumbers.get(iValue) + 1;
                mNumbers.put(iValue, iActualizarFrecuencia);
                iMax = iActualizarFrecuencia > iMax ? iActualizarFrecuencia : iMax;
            } else {
                mNumbers.put(iValue, 1);
            }
        }
        for (Map.Entry<Integer, Integer> entry : mNumbers.entrySet()) {
            Integer value = entry.getValue();
            if (value.intValue() == iMax) {
                iCounter++;
            }
        }
    }

    public int sumaDeDigitos(int iValue) {
        String sValue = String.valueOf(iValue);
        int iSum = 0;
        for (char c : sValue.toCharArray()) {
            iSum += Integer.valueOf("" + c);
        }
        return iSum;
    }
}
