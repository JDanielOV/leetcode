package leetcode;

public class ReplaceAllDigitsWithCharacters1844 {
    public static void main(String[] args) {
        ReplaceAllDigitsWithCharacters1844 v = new ReplaceAllDigitsWithCharacters1844();
        System.out.println(v.replaceDigits("a1b2c3d4e"));
    }

    public String replaceDigits(String s) {
        StringBuilder sbText = new StringBuilder(s);
        for (int i = 1; i < sbText.length(); i += 2) {
            String sNumber = "" + sbText.charAt(i);
            char cLetter = sbText.charAt(i - 1);
            char cNewLetter = (char) (cLetter + Integer.parseInt(sNumber));
            sbText.replace(i, i + 1, cNewLetter + "");
        }
        return sbText.toString();
    }
}
