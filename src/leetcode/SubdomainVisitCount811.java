/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Daniel Ochoa "901 mail.com","50 yahoo.com","900 google.mail.com","5
 * wiki.org","5 org","1 intel.mail.com","951 com"
 */
public class SubdomainVisitCount811 {

    public void SubdomainVisitCount811() {
        String[] cpdomains = {"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"};
        Map<String, Integer> mList = new HashMap();
        ArrayList<String> aListReturn = new ArrayList<>();
        for (String cpdomain : cpdomains) {
            ArrayList<String> aListSeparator = separator(cpdomain.substring(cpdomain.indexOf(" ") + 1));
            int iPoints = Integer.parseInt(cpdomain.substring(0, cpdomain.indexOf(" ")));
            for (String string : aListSeparator) {
                if (mList.containsKey(string)) {
                    mList.put(string, mList.get(string) + iPoints);
                } else {
                    mList.put(string, iPoints);
                }
            }
        }
        for (Map.Entry<String, Integer> entry : mList.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            aListReturn.add(value + " " + key);
        }
    }

    public ArrayList<String> separator(String sValue) {
        ArrayList<String> aList = new ArrayList();
        StringBuilder sText = new StringBuilder(sValue);
        int iPosPoint = sText.indexOf(".");
        System.out.println(iPosPoint);
        while (iPosPoint != -1) {
            aList.add(sText.toString());
            sText.delete(0, iPosPoint + 1);
            iPosPoint = sText.indexOf(".");
        }
        if (sText.length() > 0) {
            aList.add(sText.toString());
        }
        return aList;
    }
}
