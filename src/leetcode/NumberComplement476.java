/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

/**
 *
 * @author Daniel Ochoa
 */
public class NumberComplement476 {

    public void NumberComplement476() {
        int num = 254;
        String sValue = Integer.toBinaryString(num);
        System.out.println(sValue);
        System.out.println(cambiarValores(sValue));
        System.out.println(convertirDecimal(cambiarValores(sValue)));
    }

    public String cambiarValores(String sValue) {
        sValue = sValue.replaceAll("0", "2");
        sValue = sValue.replaceAll("1", "0");
        sValue = sValue.replaceAll("2", "1");
        return sValue;
    }

    public int convertirDecimal(String sValue) {
        StringBuilder sValor = new StringBuilder(sValue).reverse();
        int iSum = 0;
        System.out.println(sValor);
        for (int i = 0; i < sValor.length(); i++) {
            if (sValor.charAt(i) == '1') {
                iSum += Math.pow(2, i);
            }
        }
        return iSum;
    }
}
