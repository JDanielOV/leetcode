/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.Arrays;

/**
 *
 * @author Daniel Ochoa
 */
public class NumberOfRectanglesThatCanFormTheLargestSquare1725 {

    public void NumberOfRectanglesThatCanFormTheLargestSquare1725() {
        int[][] rectangles = {{5, 8}, {3, 9}, {5, 12}, {16, 5}};
        int[] iLengths = new int[rectangles.length];
        int i = 0;
        int iLengthMax = 0;
        for (int[] rectangle : rectangles) {
            iLengths[i] = Math.min(rectangle[0], rectangle[1]);
            i++;
        }
        i = 0;
        Arrays.sort(iLengths);
        iLengthMax = iLengths[iLengths.length - 1];
        for (int iLength : iLengths) {
            if (iLengthMax == iLength) {
                i++;
            }
        }
        System.out.println(i);
    }
}
