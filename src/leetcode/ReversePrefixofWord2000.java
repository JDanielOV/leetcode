package leetcode;

public class ReversePrefixofWord2000 {
    public static void main(String[] args) {
        ReversePrefixofWord2000 r = new ReversePrefixofWord2000();
        System.out.println(r.reversePrefix("abcdefd", 'd'));
        System.out.println(r.reversePrefix("abcd", 'z'));
    }

    public String reversePrefix(String word, char ch) {
        int iPosicion = word.indexOf(ch);
        String sPrimeraParte = word.substring(0, iPosicion + 1);
        String sSegundaParte = word.substring(iPosicion + 1);
        return new StringBuilder(sPrimeraParte).reverse().toString().concat(sSegundaParte);
    }
}
