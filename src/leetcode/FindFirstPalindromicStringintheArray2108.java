package leetcode;

public class FindFirstPalindromicStringintheArray2108 {
    public static void main(String[] args) {
        FindFirstPalindromicStringintheArray2108 v = new FindFirstPalindromicStringintheArray2108();
        System.out.println(v.firstPalindrome(new String[]{"abc", "car", "cool"}));
    }

    public String firstPalindrome(String[] words) {
        for (String s :
                words) {
            if (s.equals(new StringBuilder(s).reverse().toString())) {
                return s;
            }
        }
        return "";
    }
}
