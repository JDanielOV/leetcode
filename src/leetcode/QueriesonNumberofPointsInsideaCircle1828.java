package leetcode;

public class QueriesonNumberofPointsInsideaCircle1828 {
    public static void main(String[] args) {
        int[][]points={{1,3},{3,3},{5,3},{2,2}};
        int[][]queries={{2,3,1},{4,3,1},{1,1,2}};
        for (int i:countPoints(points,queries)) {
            System.out.println("i = " + i);
        }
    }

    public static int[] countPoints(int[][] points, int[][] queries) {
        int[] iOutputs = new int[queries.length];
        int k=0;
        for (int[] i : queries) {
            int iCounter = 0;
            for (int[] point : points) {
                double d = Math.sqrt(Math.pow((i[0] - point[0]), 2) + Math.pow((i[1] - point[1]), 2));
                System.out.println("d = " + d);
                if (d <= i[2]) {
                    iCounter++;
                }
            }
            iOutputs[k++]=iCounter;
        }
        return iOutputs;
    }
}
