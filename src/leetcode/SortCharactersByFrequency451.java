package leetcode;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class SortCharactersByFrequency451 {
    public static void main(String[] args) {

    }

    public String frequencySort(String s) {
        HashMap<Character, Integer> hmMapa = new HashMap<>();
        ArrayList<frequency> afrec = new ArrayList<>();
        StringBuilder sbText = new StringBuilder();
        char[] cValues = s.toCharArray();
        for (char c : cValues) {
            if (!hmMapa.containsKey(c)) {
                hmMapa.put(c, 1);
            } else {
                hmMapa.put(c, hmMapa.get(c) + 1);
            }
        }
        frequency fValue;
        for (char c : hmMapa.keySet()) {
            fValue = new frequency(c, hmMapa.get(c));
            afrec.add(fValue);
        }
        Comparator<frequency> v = (f1, f2) -> f1.iValue - f2.iValue;
        afrec.sort(v);
        for (frequency f : afrec) {
            int i = f.iValue;
            for (int j = 0; j < i; j++) {
                sbText.append(f.cValue);
            }
        }
        return sbText.toString();
    }

    class frequency {
        char cValue;
        int iValue;

        public frequency(char cValue, int iValue) {
            this.cValue = cValue;
            this.iValue = iValue;
        }
    }
}
