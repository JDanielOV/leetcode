package leetcode;

public class MinimumOperationsToMakeTheArrayIncreasing1827 {
    public static void main(String[] args) {
        MinimumOperationsToMakeTheArrayIncreasing1827 v = new MinimumOperationsToMakeTheArrayIncreasing1827();
        int[] i = {1,5,2,4,1};
        System.out.println(v.minOperations(i));
    }

    public int minOperations(int[] nums) {
        int iOperations = 0;
        for (int i = 1; i < nums.length; i++) {
            int iValueBefore = nums[i - 1];
            int iValueAfter = nums[i];
            if (iValueBefore >= iValueAfter) {
                int iValueDif = iValueBefore - iValueAfter;
                iOperations += (iValueDif + 1);
                nums[i] += iValueDif + 1;
            }
        }
        for (int i : nums) {
            System.out.println(i);
        }
        return iOperations;
    }
}
