/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Daniel Ochoa
 */
public class UniqueNumberofOccurrences1207 {

    public void UniqueNumberofOccurrences1207() {
        int[] arr = {1, 2, 2, 1, 1, 3, 5, 6, 6};
        Map<Integer, Integer> mListNumbers = new HashMap();
        long iDistin = 0;
        for (int i : arr) {
            if (mListNumbers.containsKey(i)) {
                mListNumbers.put(i, mListNumbers.get(i) + 1);
            } else {
                mListNumbers.put(i, 1);
            }
        }
        iDistin = mListNumbers.values().stream().distinct().count();
        System.out.println(iDistin);
    }
}
