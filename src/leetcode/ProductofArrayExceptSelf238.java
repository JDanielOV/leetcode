package leetcode;

import java.util.Arrays;

public class ProductofArrayExceptSelf238 {
    public int[] productExceptSelf(int[] nums) {
        int iMultipiclacion = 1,iContadorCeros=0;
        for (int i : nums) {
            if (i != 0)
                iMultipiclacion *= i;
            else
                iContadorCeros++;
        }
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0 && iContadorCeros==1) {
                System.out.println("entra 1 ");
                nums[i] = iMultipiclacion;
            } else if (nums[i] == 0 && iContadorCeros>1) {
                System.out.println("entra 2");
                nums[i] = 0;
            } else if (nums[i] != 0 && iContadorCeros>0) {
                System.out.println("entra 3");
                nums[i] = 0;
            } else if (nums[i] != 0 && iContadorCeros==0) {
                System.out.println("entra 4");
                nums[i] = iMultipiclacion / nums[i];
            }
        }
        this.imprimir(nums);
        return nums;
    }

    public void imprimir(int[] nums) {
        for (int i :
                nums) {
            System.out.print(i + " ");
        }
    }

    public static void main(String[] args) {
        int[] nums = {0,4,0};
        ProductofArrayExceptSelf238 v = new ProductofArrayExceptSelf238();
        v.productExceptSelf(nums);
    }
}
