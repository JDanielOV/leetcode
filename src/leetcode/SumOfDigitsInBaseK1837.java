package leetcode;

public class SumOfDigitsInBaseK1837 {
    public static void main(String[] args) {
        SumOfDigitsInBaseK1837 v = new SumOfDigitsInBaseK1837();
        System.out.println(v.sumBase(34, 6));
    }

    public int sumBase(int n, int k) {
        String sValue = (Integer.toString(n, k));
        int iValue = 0;
        for (char c : sValue.toCharArray()) {
            iValue += Integer.parseInt("" + c);
        }
        return iValue;
    }
}
