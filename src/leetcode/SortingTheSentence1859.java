package leetcode;

import java.util.Map;
import java.util.HashMap;

public class SortingTheSentence1859 {

    public void sortSentence() {
        String s = "is2 sentence4 This1 a3";
        String[] sWords = s.split(" ");
        HashMap<Integer, String> hmOrdens = new HashMap<>();
        StringBuilder sbOrderWords = new StringBuilder();
        for (String i : sWords) {
            int iValue = Integer.parseInt(i.substring(i.length() - 1));
            String sWord = i.substring(0, i.length() - 1);
            hmOrdens.put(iValue, sWord);
        }
        for (String i : hmOrdens.values()) {
            sbOrderWords.append(i);
            sbOrderWords.append(" ");
        }
        sbOrderWords.deleteCharAt(sbOrderWords.length() - 1);
        System.out.println(sbOrderWords + "#");
    }

    public static void main(String[] args) {
        SortingTheSentence1859 v = new SortingTheSentence1859();
        v.sortSentence();
    }
}
