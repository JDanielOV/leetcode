package leetcode;

import java.util.Arrays;

public class MaximumNumberofCoinsYouCanGet1561 {
    public static void main(String[] args) {
        MaximumNumberofCoinsYouCanGet1561 m = new MaximumNumberofCoinsYouCanGet1561();
        System.out.println(m.maxCoins(new int[]{3, 7, 5, 2, 9, 3, 3, 8, 1}));
        System.out.println(m.maxCoins(new int[]{2, 4, 1, 2, 7, 8}));
        System.out.println(m.maxCoins(new int[]{2, 4, 5}));
        System.out.println(m.maxCoins(new int[]{9, 8, 7, 6, 5, 1, 2, 3, 4}));
    }

    public int maxCoins(int[] piles) {
        Arrays.sort(piles);
        int iContadorPilaMaxima = 0;
        for (int i = (piles.length / 3); i < piles.length - 1; i += 2) {
            iContadorPilaMaxima += piles[i];
        }
        return iContadorPilaMaxima;
    }
}
