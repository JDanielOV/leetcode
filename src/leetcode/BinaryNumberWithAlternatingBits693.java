package leetcode;

public class BinaryNumberWithAlternatingBits693 {
    public static void main(String[] args) {
        System.out.println(hasAlternatingBits(11));
    }
    public static boolean hasAlternatingBits(int n) {
        String sValue = Integer.toBinaryString(n);
        System.out.println("sValue = " + sValue);
        return !(sValue.contains("11") || sValue.contains("00"));
    }
}
